import { Login } from '@/api/login.js';
const state = {};
const getters = {};
const mutations = {};
const actions = {
  loginSyn (content, requestData) {
    return new Promise((resolve, reject) => {
      Login(requestData)
        .then(response => {
          resolve(response);
        }).catch(error => {
          reject(error);
        })
    });
  }
};
export default {
  namespaced: true, // 启用命名空间模式
  state,
  mutations,
  getters,
  actions
};
