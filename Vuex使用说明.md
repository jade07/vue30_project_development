# VUEX的使用说明
## 概述
>  vuex是一个专为vue.js应用程序开发的状态管理模式，vuex解决了组件之间同一状态的共享问题。
- state 
  > 存储初始化数据 可通过`this.$store.state.xxxx`读取
- getters
  > 对state中的数据进行二次处理（对数据进行过滤，类似filter的作用）如state返回的值时一个对象时，而需要拿对象中指定键名的值时，可以使用`this.$store.getters.xxx`
- mutations
  > 对数据进行计算的方法可以写在里面（类似computed)，在页面中触发时使用`this.$store.commit('mutationName')`来触发对应的mutationName方法进行修改state的值；
- actions
  > action的功能和mutation时类似的，都可以改变strore中的state中初始的数据，有两个不同点
  >- action主要处理的时异步的操作，mutation必须时同步执行的，action则可以处理同步和异步。
  >- action改变状态，最后时通过提交mutation `this.$store.dispatch(actionName)`
  >- 角色定位基于流程顺序，两者扮演不同的角色。muation专注修改state，理论上时修改state的唯一途径；action专注业务代码、异步操作
- modules
  > 可以实现模块化使用，类似vue中的模块管理
  ```js
  export default new Vuex.Store({
    modules:{
      login,
      app,
      user
    }
  })
  ```
图示
![QQ图片20200323185230.png](./images/QQ图片20200323185230.png)

安装
```js
npm install vuex --save
```
代码结构
```js
import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {}
});


```

## `state`存储和`getters`获取
vuex存储端
```js
import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isOpen: false,
    count: 10 
  },
  getters: {
    // 对count数据进行+10处理再输出
    count: (state) => {
      return state.count + 10;
    },
    // 或者
    count: state => state.count + 10
  }
});
```
vue读取端
```js
export default {
  data () {
    // 通过state方式获取 
    console.log(this.$store.state.isOpen); // 输出结果 false

    // 可以预先对数据进行加工处理在输出
    console.log(this.$store.getters.count);
    return {
    };
  }
}
```

## `mutations`转变方法
vuex存储端
```js
export default new Vuex.Store({
  state: {
    count: 10
  },
  mutations: {
    setCount(state,value){
      state.count = value;
    }
  },
  actions: {
  },
  modules: {
  }
})
```
vuex读取端
```js
export default {
  name: "Home",
  data () {
    // 初始化输出的值为20
    console.log(this.$store.state.count);
    // 通过该方式对store中的数据进行修改
    this.$store.commit('setCount',100);
    // 修改后的值为 100
    console.log(this.$store.state.count);

    return {

    };
  }
}
```

## `actions`修改数据
### 同步方式
vuex存储端
```js
export default new Vuex.Store({
  state: {
    isOpen: false,
    count: 10
  },
  getters: {
    count: (state) => {
      return state.count + 10;
    },
    // 或者
    count: state => state.count + 10,

  },
  mutations: {
    setCount (state, value) {
      state.count = value;
    }
  },
  actions: {
    setCountAction (content, data) {
      //content 中包含state,getters,commit,rootGetters和rootState对象
      content.commit("setCount",1000);
    }
  },
  modules: {
  }
})
```
vue读取端
```js
export default {
  name: "Home",
  data () {
    // 初始输出为20 
    console.log(this.$store.getters.count);
    // 通过该方式对store中的数据进行修改
    this.$store.commit('setCount',100);
    // 输出为100
    console.log(this.$store.state.count);
    // 通过该方式调用vuex中actions中的setCountAction方法,再通过该方法内部调用mutations中的方法进行state值修改
    this.$store.dispatch('setCountAction');
    // 最终输出为1000
    console.log(this.$store.state.count);
    return {

    };
  }
}
```

### 异步方式

vuex存储端
```js
export default new Vuex.Store({
  state: {
    token：''
  },
  getters: {},
  mutations: {},
  actions: {
    LoginSyn (content, requestData) {
      return new Promise((resolve, reject) => {
        // 调用一个异步接口
        Login(requestData)
        .then((response)=>{
          // 可以对接口返回的数据进行vuex存储处理
          content.$state.token = response.data.token;
          resolve(response);
        })
        .catch(error=>{
          reject(error);
        })
      })
    }
  },
  modules: {
  }
})


```
vue读取端
```js
export default {
  data () {
    // 调接口
    this.$store.dispatch('loginSyn', data)
    .then(response=>{
      // 根据接口返回的结果进行处理
    }).catch(error=>{

    });
    return {

    };
  }
}
```

## `modules`模块
> 在store模块中的新建对应的模块，如`login.js`、`user.js`等
vuex主入口文件`index.js`
```js
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

// 引入模块
import app from "./app.js";
import login from "./login.js";
import user from "./user.js";
...

export default new Vuex.Store({
  modules: {
    app,
    login,
    user,
    ...
  }
})

```
各模块文件的数据
### 没有使用命名空间的方式 (存在命名冲突的问题)
`app.js`
```js
const app = {
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  }
};

export default app;

```
`login.js`
```js
```js
const login = {
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  }
};

export default login;

```
...
vue读取端
> 读取时对应的方法要增加对应的模块名称
- `this.$store.getters.模块名称.对应的键值`
- `this.$store.state.模块名称.对应的键值`
- `this.$store.commit.方法名称`
- `this.$store.dispatch.模块名称.方法名称`


各模块文件的数据
### 没有使用命名空间的方式 (存在命名冲突的问题)
`app.js`
```js
const state = {};
const getters = {};
const mutations = {};
const actions  = {};

export default {
  namespaced: true, // 启用命名空间模式
  state,
  getters,
  mutations,
  actions
};

```
`login.js`
```js
const state = {};
const getters = {};
const mutations = {};
const actions  = {};

export default {
  namespaced: true, // 启用命名空间模式
  state,
  getters,
  mutations,
  actions
};

```
...
vue读取端
> 读取时对应的方法要增加对应的模块名称
- `this.$store.getters.模块名称.对应的键值`
- `this.$store.state.模块名称.对应的键值`
- `this.$store.commit(模块名称/方法名称)`
- `this.$store.dispatch(模块名称/方法名称)`