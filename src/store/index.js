import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
import app from "./modules/app"; // app配置
import user from "./modules/user"; // login模块配置

export default new Vuex.Store({
  modules: {
    app,
    user
  }
});
