import Vue from "vue";
import VueRouter from "vue-router";
import NProgress from "nprogress";

// 引入布局组件
import Layout from '@/views/layout/Index.vue';

Vue.use(VueRouter);

let routes = [
  {
    path: "/",
    redirect: "home",
    hidden: true,
    meta: {
      icon: "el-icon-user-solid",
      name: "主页"
    }
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("@/views/Login.vue"),
    hidden: true,
    meta: {
      name: "登录"
    }
  },
  {
    path: "/home",
    name: "Home",
    component: Layout,
    hidden: false,
    meta: {
      icon: "el-icon-menu",
      name: "控制台"
    },
    children: [
      {
        path: "/home",
        name: "Home",
        component: () => import("@/views/home/Index.vue"),
        hidden: false,
        meta: {
          name: "首页"
        }
      }
    ]
  },
  /**
   * 资讯栏目
   */
  {
    path: "/news",
    name: "News",
    component: Layout,
    hidden: false,
    meta: {
      icon: "el-icon-news",
      name: "资讯管理"
    },
    children: [
      {
        path: "/newsIndex",
        name: "NewsIndex",
        component: () => import("@/views/news/Index.vue"),
        hidden: false,
        meta: {
          name: "资讯列表"
        }
      },
      {
        path: "/newsCategory",
        name: "NewsCategory",
        component: () => import("@/views/news/Category.vue"),
        hidden: false,
        meta: {
          name: "资讯分类"
        }
      }
    ]
  },
  {
    path: "/user",
    name: "User",
    component: Layout,
    hidden: false,
    meta: {
      icon: "el-icon-user-solid",
      name: "用户管理"
    },
    children: [
      {
        path: "/userIndex",
        name: "UserIndex",
        component: () => import("@/views/user/Index.vue"),
        hidden: false,
        meta: {
          name: "用户列表"
        }
      }
    ]
  },
  {
    path: "/setting",
    name: "Setting",
    component: Layout,
    hidden: false,
    meta: {
      icon: "el-icon-s-tools",
      name: "站点设置"
    },
    children: [
      {
        path: "/settingIndex",
        name: "SettingIndex",
        component: () => import("@/views/setting/Index.vue"),
        hidden: false,
        meta: {
          name: "基本配置"
        }
      }
    ]
  }
];

const router = new VueRouter({
  routes
});

// 动态增加路由
router.addRoutes([

]);

// 导航守卫
router.beforeEach((to, from, next) => {
  NProgress.start();
  next();
});

router.afterEach(() => {
  NProgress.done();
});

export default router;
