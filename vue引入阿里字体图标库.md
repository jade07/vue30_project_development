# 引入阿里字体图标库
## 准备工作
> 通过自定义模板文件方式，进行引入，需要注意vue运行模式问题。vue默认运行的模式位runtime模式（运行时），需要改成compiler模式（模板）
> vue的js文件默认指向`./node_modules/vue/dist/vue.runtime.common.js'位置，需要在 `vue.config.js`中配置，修改指引位置，改为指向`./node_modules/vue/dist/vue.js` 。

![QQ图片20200323103959.png](./images/QQ图片20200323103959.png)

修改`vue.config.js`配置文件
`'vue': 'vue/dist/vue.js',`
```js
configureWebpack: (config) => {
        if (process.env.NODE_ENV === 'production') {
            // 为生产环境修改配置...
            config.mode = 'production'
        } else {
            // 为开发环境修改配置...
            config.mode = 'development'
        }
        Object.assign(config, {
            // 开发生产共同配置
            resolve: {
                alias: {
                    'vue': 'vue/dist/vue.js',
                    '@': path.resolve(__dirname, './src'),
                    '@c': path.resolve(__dirname, './src/components'),
                    '@p': path.resolve(__dirname, './src/pages')
                } // 别名配置
            }
        })
    }
```



![QQ图片20200323103643.png](./images/QQ图片20200323103643.png)
### 模板定义规则：
- 在入口文件`main.js`中引入自定义模板，即可作为全局模板使用
`Vue.component("组件名称",{"组件代码"})
```
// 自定义模板
Vue.component('svg-icon',{
  template:"<div>{{msg}}</div>",
  data(){
    return {
      msg: "阿里图标库"
    }
  }
})
```

## 开始配置阿里图标库
- 在src目录下新建阿里图标库文件夹`iconfont`，然后再文件里面创建一个`svg`文件夹，保存`.svg`文件
- 新建`index.js`入口文件和`SvgIcon.vue`模板文件
- 然后再项目入口文件中引入`import "./iconfont/index.js"`或`import "./iconfont"`;
- 配置`vue.config.js`文件，用于支持解析`svg`文件
- 安装依赖包 `npm install svg-sprite-loader -S`
- 需要使用图标的位置进行使用`<svg-icon icon="menu" class="font50"></svg-icon>`
- 重启项目

`index.js`入口文件内容
```js
import Vue from "vue";
import SvgIcon from "./SvgIcon.vue";
Vue.component('svg-icon',SvgIcon);
```
`SvgIcon.vue`模板文件内容
```vue
<template>
  <svg :class="iconClass"  aria-hidden="true">
    <use :xlink:href="iconName"></use>
  </svg>
</template>

<script>
import { reactive, ref, isRef, toRefs, onMounted, computed } from '@vue/composition-api';
export default {
  name: "SvgIcon",
  props: [
    'icon', 'class'
  ],
  setup (props, { }) {
    console.log(props);

    // 计算属性
    const iconName = computed(() => `#icon-${props.icon}`);
    const iconClass = computed(() => {
      if(props.class){
        return `svg-icon ${props.class}`;
      }else{
        return `svg-icon`;
      }
    });

    return {
      iconName,
      iconClass
    }
  }
}
</script>

<style>
</style>


```

`main.js`入口文件引入
```js
// 引入自定义公共模板文件-阿里图标库
import "./iconfont";
```
`vue.config.js`配置文件

```js
// webpack配置 
chainWebpack: (config) => {
    const svgRule = config.module.rule("svg");
    svgRule.uses.clear();
    svgRule
    .use("svg-sprite-loader")
    .loader("svg-sprite-loader")
    .options({ 
        symbolId: "icon-[name]",
        include: ["./src/iconfont"] 
    });
},
```

使用图标模板组件
- `icon="图标名称"` 对应svg文件夹中对应的图标名称
```
<svg-icon icon="menu" class="font50"></svg-icon>
```

修改图标的颜色
- 注意需要增加`fill: currentColor;`才能修改颜色
- 也可以增夹对应的自定义类，如果`font50`设置字体大小
```
.svg-icon {
  width: 1em;
  height: 1em;
  fill: currentColor;
  color: #f00;
  margin-left: 50px;
  &.font50{
    font-size: 50px;
  }
}
```
