import Cookie from "cookie_js";

/**
* 自動保留兩位小數
* @param value
* @returns {string|number}
*/
export function returnFloat (value) {
    var value = Math.round(parseFloat(value) * 100) / 100;
    var xsd = value.toString().split(".");
    if (xsd.length == 1) {
        value = value.toString() + ".00";
        return value;
    }
    if (xsd.length > 1) {
        if (xsd[1].length < 2) {
            value = value.toString() + "0";
        }
        return value;
    }
}

const adminToken = "admin_token";
export function getToken (type="cookie") {
    if(type=="cookie"){
        return Cookie.get(adminToken);
    }else{
        return localStorage.getItem(adminToken);
    }
}

export function setToken (value,type="cookie") {
    if(type=="cookie"){
       Cookie.set(adminToken,value);
    }else{
       localStorage.getItem(adminToken,value);
    }
}

export function getUsername (type="cookie") {
    if(type=="cookie"){
        return Cookie.get("username");
    }else{
        return localStorage.getItem("username");
    }
}

export function setUsername (value,type="cookie") {
    if(type=="cookie"){
       Cookie.set("username",value);
    }else{
       localStorage.getItem("username",value);
    }
}

export function removeToken(type="cookie") {
    if(type=="cookie"){
       Cookie.remove(adminToken);
    }else{
       localStorage.remove(adminToken);
    }
}

export function removeUsername(type="cookie") {
    if(type=="cookie"){
       Cookie.remove("username");
    }else{
       localStorage.remove("username");
    }
}
