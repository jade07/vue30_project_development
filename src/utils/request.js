import axios from 'axios';
import { Message } from 'element-ui';

// console.log(process.env.NODE_ENV);
const baseURL = process.env.NODE_ENV === 'production' ? '' : '/devApi';
// 创建axios,赋值位service
const service = axios.create({
  baseURL: baseURL,
  timeout: 10000
});

// 添加请求拦截器
service.interceptors.request.use(function (config) {
  return config;
}, function (error) {
  // 请求错误时
  return Promise.reject(error);
});

// 添加响应拦截器
service.interceptors.response.use(function (response) {
  let data = response.data;
  if (data.resCode !== 200) {
    Message.error(data.message);
    return Promise.reject(data);
  }else{
    // 对响应结果进行处理
    return response;
  }
}, function (error) {
  // 响应错误
  return Promise.reject(error);
})

export default service;
