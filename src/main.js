import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VueCompositionApi from '@vue/composition-api';
Vue.use(VueCompositionApi);

// 引入自定义公共模板文件-阿里图标库
import "./iconfont";

// 引入ElementUI
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(ElementUI);


// 引入导航守卫文件
import "./router/premit";



// 引入公共组件
import "./components";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
