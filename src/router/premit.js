import router from "./index";
import store from "../store";
import { getToken, removeToken,removeUsername } from "../utils/app";
// 进行路由守卫进行登录判断
// 路由守卫白名单设置
const whiteRouter = ["/login"];
router.beforeEach((to, from, next) => {
  // console.log(to,from,next);
  // 判断本地存储中是否存储token,不存在则跳转到login页面
  // console.log(getToken);
  if (getToken()) {
    if (to.path === "/login") {
      // 删除cookie
      removeToken();
      removeUsername();
      // 删除store中的存储
      store.commit("app/setToken","");
      store.commit("app/setUsername","");
    }

    // 角色
    // 动态路由

    next();
  } else {
    // 判断需要进入的页面是否为白名单中的页面,
    // 如果是则直接进入，如果没有改步判断，会一直处于死循环中
    if (whiteRouter.indexOf(to.path) !== -1) {
      next();
    } else {
      next("/login");
    }
  }
});