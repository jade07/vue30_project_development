/**
 * 过滤特殊字符 
 * @param {字符串} s 
 */
export function stripscript (s) {
  var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？ ]")
  var rs = "";
  for (var i = 0; i < s.length; i++) {
    rs = rs + s.substr(i, 1).replace(pattern, '');
  }
  return rs;
} 


/**
 * 電話號碼規則檢查，檢查香港和大陸手機號碼
 * @param value
 * @returns {boolean|*}
 */
export function checkMobileNumber(value,type){
  var reg1 = /^((13[0-9])|(15[^4])|(18[0,2,3,5-9])|(17[0-8])|(147))\d{8}$/;//大陸手機正則
  var reg2 = /^(3|4|5|6|8|9)\d{7}$/;//香港手機正則
  if(type==1){
      if(reg1.test(value)) {
          return true;
      }
  }else if(type==2){
      if(reg2.test(value)) {
          return true;
      }
  }else{
      if(reg1.test(value) || reg2.test(value)) {
          return true;
      }
  }

  return false;
}



/**
 * 检查邮箱
 * @param {string} email 
 */
export function checkEmail(email){
  var reg = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
  if(!reg.test(email)) {
      return false;
  }
  return true;

}

