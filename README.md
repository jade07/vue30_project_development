# VUE3.0项目开发流程

## 环境搭建

- Node环境安装 安装地址： https://nodejs.org/en/download/ 

  > 查看版本号 `node -V`和`npm -v`出现版本号即可，要求版本node8.9或以上版本，安装nodejs时，会同时安装`npm`包管理工具

- 管理nodejs版本（<span style="color:red">可以不安装</span>）

  > 执行安装命令：`npm install -g n`
  >
  > n lastest (把nodejs升级到最新版本)
  >
  > n stable (把nodejs升级到最新稳定版本)
  >
  > n 后面也可以跟随版本号，如：`n v0.10.26`或`n 0.10.26`

- 全局安装VUE3.0脚手架

  > 卸载：如果已经全局安装了旧版本的vue-cli(1.x或2.x)，需要先卸载：`npm uninstall vue-cli -g`
  >
  > 安装：`npm install -g @vue/cli`
  >
  > 查看版本： `vue -V`，出现版本号，即安装成功。如果没有显示，可以先重启电脑看。
  >
  > 兼容2.0：`npm install -g @vue/cli-init` 安装4.0版本对2.0版本桥接

- 安装淘宝镜像 cnpm

  > 由于npm包源在国外，可以安装国内的镜像进行提速
  >
  > `npm install cnpm -g --registry=http://registry.npm.taobao.org`
  >
  > 也可以设置npm包源
  >
  > `npm config set registry http://registry.npm.taobao.org`

## 建立项目仓库（github 或gitee）

- github(gitee)

  > github：https://github.com/
  >
  > gitee： https://gitee.com/ 

- git基础命令
  - 拷贝项目：`git clone <仓库地址>`
  - 创建分支：`git branch <name>`
  - 创建并进入分支：`git checkout -b <name>`
  - 查看分支：`git branch --list`
  - 查看分支（含远程分支）：`git branch -a`
  - 查看状态：`git status`
  - 添加所有文件：`git add .`
  - 提交文件： `git commit -m '提交内容描述或备注'`
  - 从远程仓库拉取内容：`git pull`
  - 推送内容到远程仓库：`git push origin <分支名称>`

- 工作目录中分支管理

  ​			![QQ图片20200318105411](./images/QQ图片20200318105411.png)

  - master： 主分支，一般不在该分支上开发项目
  - dev：开发分支
  - 版本分支：建立在dev分支基础上
    - feature-vueAdmin-V1.0.0-20190919：分支完整名称
    - feature：描述当前分支类型
    - vueAdmin：项目名称
    - V1.0.0：版本号
    - 20200317：建立分支日期
  - BUG分支：建立于当前版本分支下面
    - bug-101241-20191020：bug分支完整名称
    - bug: 分支类型
    - 101241：bug的ID
    - 20190919：建立分支日期

- 与gitee仓库为例
  
  - 登录gitee创建一个空项目,然后拷贝
  
    ![QQ图片20200318111638.png](./images/QQ图片20200318111638.png)
  
  - 本地选择项目开发位置把项目克隆到本地 `git clone 远程项目地址`
  
    ![QQ图片20200318111944.png](./images/QQ图片20200318111944.png)
  
  - 创建项目分支`git checkout -b dev`
  
    ```git 
    $ git checkout -b dev
    Switched to a new branch 'dev'
    rufeike@DESKTOP-EGQ6CJV MINGW64 /e/vue/vue30_project_development (dev)
    
    ```
  
  - 把分支推送到远程服务器 `git push` 再执行 `git push --set-upstream origin dev`
  
    > 注意，在把分支推送到远程服务器时，如果分支中有新内容，推送时会报以下错误,可以先把内容使用`git add` 和`git commit`命令添加，后再推送
  
    ![QQ图片20200318112913.png](./images/QQ图片20200318112913.png)
  

## 构建VUE3.0项目
- 进入已经部署远程仓库的项目目录中，使用vue项目构造命令
`vue create .`  

![QQ图片20200318114548.png](./images/QQ图片20200318114548.png)

- 进行项目配置设置，更加需要进行选择，默认自由系统默认配置和自定义配置两个选项，如果个人之前已经保存个配置记录，会有多个配置项选择。

![QQ图片20200318114826.png](./images/QQ图片20200318114826.png)

- 上一步选择自定义配置项后，进入以下提醒项目，更加需要选中需要的配置项目

![QQ图片20200318115414.png](./images/QQ图片20200318115414.png)

- 是否选中history模式，选中y的话，项目打包后，地址栏中会去掉#号标识，但需要后端配合设置。一般开发过程中，选中n

![QQ图片20200318115758.png](./images/QQ图片20200318115758.png)

- 选中css样式支持，根据项目需求可以自由选中。本次选中`node-sass`

![QQ图片20200318120116.png](./images/QQ图片20200318120116.png)

- 选择代码规范检查模式，根据公司需要进行选择

![QQ图片20200318120233.png](./images/QQ图片20200318120233.png)

- 什么情况下检查代码，默认第一项就好

![QQ图片20200318120450.png](./images/QQ图片20200318120450.png)

- 选择不同配置项保存方式，可以单独保存或合并保存到packpage.json中，选择单独配置

![QQ图片20200318120707.png](./images/QQ图片20200318120707.png)

- 是否保存本次配置项，如果保存，下次可以直接使用

![QQ图片20200318120828.png](./images/QQ图片20200318120828.png)

- 确认后，进入等待安装。

![QQ图片20200318120951.png](./images/QQ图片20200318120951.png)

- 安装好后，可以执行`npm run serve`运行项目




## Vue项目初始化配置修改

### Vue项目基本配置 `vue.config.js`
> 在项目更目录中，新建`vue.config.js`文件，内容如下，用于配置项目的相关配置。
```js
const path = require('path')

module.exports = {
    publicPath: './', // 基本路径
    outputDir: 'dist', // 输出文件目录
    lintOnSave: true, // eslint-loader 是否在保存的时候检查
    // see https://github.com/vuejs/vue-cli/blob/dev/docs/webpack.md
    // webpack配置
    chainWebpack: (config) => {
    },
    configureWebpack: (config) => {
        if (process.env.NODE_ENV === 'production') {
            // 为生产环境修改配置...
            config.mode = 'production'
        } else {
            // 为开发环境修改配置...
            config.mode = 'development'
        }
        Object.assign(config, {
            // 开发生产共同配置
            resolve: {
                alias: {
                    '@': path.resolve(__dirname, './src'),
                    '@c': path.resolve(__dirname, './src/components'),
                    '@p': path.resolve(__dirname, './src/pages')
                } // 别名配置
            }
        })
    },
    productionSourceMap: false, // 生产环境是否生成 sourceMap 文件
    // css相关配置
    css: {
        extract: true, // 是否使用css分离插件 ExtractTextPlugin
        sourceMap: false, // 开启 CSS source maps?
        loaderOptions: {
            sass: {
                prependData: `@import "./src/assets/styles/main.scss";`
            }, // 这里的选项会传递给 css-loader
            // postcss: {} // 这里的选项会传递给 postcss-loader
        }, // css预设器配置项 详见https://cli.vuejs.org/zh/config/#css-loaderoptions
        modules: false // 启用 CSS modules for all css / pre-processor files.
    },
    parallel: require('os').cpus().length > 1, // 是否为 Babel 或 TypeScript 使用 thread-loader。该选项在系统的 CPU 有多于一个内核时自动启用，仅作用于生产构建。
    pwa: {}, // PWA 插件相关配置 see https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-pwa
    // webpack-dev-server 相关配置
    devServer: {
        open: process.platform === 'darwin',
        host: '0.0.0.0', // 允许外部ip访问
        port: 8080, // 端口
        https: false, // 启用https
        overlay: {
            warnings: true,
            errors: true
        }, // 错误、警告在页面弹出
        proxy: {
            '/api': {
                target: 'http://www.baidu.com/api',
                changeOrigin: true, // 允许websockets跨域
                // ws: true,
                pathRewrite: {
                    '^/api': ''
                }
            }
        } // 代理转发配置，用于调试环境
    },
    // 第三方插件配置
    pluginOptions: {}
}

```

### 设置公共CSS样式文件
- 在`./src/assets`目录下，新建`styles`目录，用于存储公共css样式文件。
- 在配置文件`vue.config.js`中配置公共css引入目录
```
// css相关配置
    css: {
        extract: true, // 是否使用css分离插件 ExtractTextPlugin
        sourceMap: false, // 开启 CSS source maps?
        loaderOptions: {
            sass: {
                prependData: `@import "./src/assets/styles/main.scss";`
            }, // 这里的选项会传递给 css-loader
            // postcss: {} // 这里的选项会传递给 postcss-loader
        }, // css预设器配置项 详见https://cli.vuejs.org/zh/config/#css-loaderoptions
        modules: false // 启用 CSS modules for all css / pre-processor files.
    },
```
![QQ图片20200318135755.png](./images/QQ图片20200318135755.png)

- 该放在该目录中的`css`文件改成后缀名为`scss`，在增加一个`main.css`作为所有css的主引入文件
  - 增加一个跨浏览器兼容性css文件`normalize.css`，改名为`normalize.scss`,并在`main.scss`中引入。
```css
@import "./normalize.scss"; 
```

### 路由修改
> 参考配置 https://github.com/rufeike/vue-web-template



### 安装 Element UI 
地址：https://element.eleme.cn/#/zh-CN

- 安装
```js
npm i element-ui -S
```

- 在main.js中完整导入
>可以按需导入和完整导入，本项目采用完整导入
```js
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

// 引入ElementUI
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(ElementUI);

// 引入公共组件
import "./components";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

```


## VUE3.0新特性语法
网址：https://vue-composition-api-rfc.netlify.com
> 需要依赖Composition API
> 安装依赖：`npm install @vue/composition-api --save`

`main.js`引入
```js
import VueCompositionApi from '@vue/composition-api';
Vue.use(VueCompositionApi);
```

### vue3.0与vue2.0明显

- reactive(声明单一对象时使用)，取的一个对象并返回原始对象的响应数据处理。
```js
const obj = reactive({count:0});
```

- ref(声明基础数据类型变量时使用)
> 内部值并返回一个响应新且可以改变ref对象。ref对象具有value指向内部值的单一属性
```js
const number = ref(0);
console.log(number.value); //取值方式

```

- `isRef`和`toRefs`
>检查一个对象是否时ref对象
```js
// isRef 举例
const unwrapped = isRef(foo)?foo.value.foo;

//toRefs举例
function useMousePosition(){
  const pos = reactive({
    x:0,
    y:0
  })

  return toRefs(pos);
}
const {x,y} = useMousePosition();

```
> toRefs 把reactive对象转为普通对象后，才能进行解构或拓展运算符不会丢失原有的响应对象的响应。

![QQ图片20200318221956.png](./images/QQ图片20200318221956.png)



## 安装 Axios
安装指令
```js
npm install axios --save
```

### 配置请求拦截工具
>新建一个工具js文件，如`request.js`
```js
import axios from 'axios';

const nodeEnv = process.env.NODE_ENV;
let baseURL = '';
if(nodeEnv == 'development' ){
  baseURL =  "https://www.rufeike.top";
}else if(nodeEnv == 'production') {
  baseURL =  "https://www.rufeike.top";// 生产开发接口
}
// 创建axios,赋值位service
const service = axios.create({
  baseURL: baseURL,
  timeout: 1000
});

// 添加请求拦截器
service.interceptors.request.use(function (config) {
  // 在发送请求之前需要做什么
  return config;
}, function (error) {
  // 请求错误时
  return Promise.reject(error);
});

// 添加响应拦截器
service.interceptors.response.use(function (response) {
  // 对响应结果进行处理
  return response;
}, function (error) {
  // 响应错误
  return Promise.reject(error);
})

export default service;

```


## 配置环境变量
> 新增两个配置文件，可以方便生产和开发不同环境的不同配置，注意配置文件修改时，需要重启项目才能读取相关配置。
- `.env.development` 开发环境变量量配置文件
- `.env.production` 生产环境变量量配置文件
```
APP_NAME = rfApp

```
>读取数据方式 `process.env.配置名称`


## vuex使用和相关配置
> 参考同目录文件 [vuex使用说明](./Vuex使用说明.md)

## 引入阿里图标或svg格式图标
> 参考同目录文件 [vue引入阿里字体图标库](./vue引入阿里字体图标库.md);

## 安装cookie_js依赖，进行cookie存取操作 ![1004543-20180206110317873-1119202380.gif](./images/1004543-20180206110317873-1119202380.gif)
> `npm install cookie_js --save`
简单用法

```js
import cookie from "cookie_js";

// 存储
cookies.set("name", "value")； // 普通设置

cookies.set("name", "value", { expires: 7, path: '' });    //   expires:7天过期 ， path: 应用的页面路径

cookies.set("name", { foo: "bar" });   //  设置一个json

// 取值
cookie.get("键"); // 获取指定键名的cookie
cookie.get(); // 获取所有

// 移除
cookie.remove("键");

```


## 引入路由守卫进行登录控制
>参考同目录文件 [路由跳转前控制-路由守卫.md](./路由跳转前控制-路由守卫.md);