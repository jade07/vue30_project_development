// 引入 moment 时间插件
import moment from 'moment'


// 获取当前月的开始结束时间
function getCurrMonthDays () {
  let obj = {
    starttime: '',
    endtime: ''
  }
  obj.starttime = moment(moment().month(moment().month()).startOf('month').valueOf()).format('YYYY-MM-DD')
  obj.endtime = moment(moment().month(moment().month()).endOf('month').valueOf()).format('YYYY-MM-DD')
  return obj
}

// 获取上一个月的开始结束时间
function getLastMonthDays () {
  let obj = {
    starttime: '',
    endtime: ''
  }
  obj.starttime = moment(moment().month(moment().month() - 1).startOf('month').valueOf()).format('YYYY-MM-DD')
  obj.endtime = moment(moment().month(moment().month() - 1).endOf('month').valueOf()).format('YYYY-MM-DD')
  return obj
}

/**
* 获取本周、本季度、本月、上月的开端日期、停止日期
*/
var now = new Date() // 当前日期
var nowDayOfWeek = now.getDay() // 今天本周的第几天
var nowDay = now.getDate() // 当前日
var nowMonth = now.getMonth() // 当前月

var nowYear = now.getYear() // 当前年
nowYear += (nowYear < 2000) ? 1900 : 0 //

// 格局化日期：yyyy-MM-dd
function formatDate (date, separator = '-') {
  var myyear = date.getFullYear()
  var mymonth = date.getMonth() + 1
  var myweekday = date.getDate()

  if (mymonth < 10) {
    mymonth = '0' + mymonth
  }
  if (myweekday < 10) {
    myweekday = '0' + myweekday
  }
  return (myyear + separator + mymonth + separator + myweekday)
}

// 获得某月的天数
function getMonthDays (myMonth) {
  myMonth = myMonth - 1
  var monthStartDate = new Date(nowYear, myMonth, 1)
  var monthEndDate = new Date(nowYear, myMonth + 1, 1)
  var days = (monthEndDate - monthStartDate) / (1000 * 60 * 60 * 24)
  return days
}

// 获得本季度的开端月份
function getQuarterStartMonth () {
  var quarterStartMonth = 0
  if (nowMonth < 3) {
    quarterStartMonth = 0
  }
  if (nowMonth > 2 && nowMonth < 6) {
    quarterStartMonth = 3
  }
  if (nowMonth > 5 && nowMonth < 9) {
    quarterStartMonth = 6
  }
  if (nowMonth > 8) {
    quarterStartMonth = 9
  }
  return quarterStartMonth
}

// // 获得本月的开端日期
// function getMonthStartDate () {
//   var monthStartDate = new Date(nowYear, nowMonth, 1)
//   return formatDate(monthStartDate)
// }

// // 获得本月的停止日期
// function getMonthEndDate () {
//   var monthEndDate = new Date(nowYear, nowMonth, getMonthDays(nowMonth))
//   return formatDate(monthEndDate)
// }

// 获得本季度的开端日期
function getQuarterStartDate () {
  var quarterStartDate = new Date(nowYear, getQuarterStartMonth(), 1)
  return formatDate(quarterStartDate)
}

// 或的本季度的停止日期
function getQuarterEndDate () {
  var quarterEndMonth = getQuarterStartMonth() + 2
  var quarterStartDate = new Date(nowYear, quarterEndMonth, getMonthDays(quarterEndMonth))
  return formatDate(quarterStartDate)
}

// 獲取相對當天 之前或之後的時間
function getDay (num, str) {
  var today = new Date()
  var nowTime = today.getTime()
  var ms = 24 * 3600 * 1000 * num
  today.setTime(parseInt(nowTime + ms))
  var oYear = today.getFullYear()
  var oMoth = (today.getMonth() + 1).toString()
  if (oMoth.length <= 1) oMoth = '0' + oMoth
  var oDay = today.getDate().toString()
  if (oDay.length <= 1) oDay = '0' + oDay
  return oYear + str + oMoth + str + oDay
}

// 获取上周起始时间结束时间、下周起始时间结束时间开始时间和本周起始时间结束时间;（西方）

/*
  参数都是以周一为基准的
  上周的开始时间
   console.log(getTime(7));
  上周的结束时间
   console.log(getTime(1));
  本周的开始时间
   console.log(getTime(0));
  本周的结束时间
   console.log(getTime(-6));
  下周的开始时间
   console.log(getTime(-7));
  下周结束时间
   console.log(getTime(-13));
*/
function getTime (n) {
  var now = new Date()
  var year = now.getFullYear()
  // 因为月份是从0开始的,所以获取这个月的月份数要加1才行
  var month = now.getMonth() + 1
  var date = now.getDate()
  var day = now.getDay()
  // console.log(date)
  // 判断是否为周日,如果不是的话,就让今天的day-1(例如星期二就是2-1)
  if (day !== 0) {
    n = n + (day - 1)
  } else {
    n = n + day
  }
  if (day) {
    // 这个判断是为了解决跨年的问题
    if (month === 0) { // 这个判断是为了解决跨年的问题,月份是从0开始的
      year = year - 1
      month = 12
    }
  }
  now.setDate(now.getDate() - n)
  year = now.getFullYear()
  month = now.getMonth() + 1
  date = now.getDate()
  // console.log(n)
  var s = year + '-' + (month < 10 ? ('0' + month) : month) + '-' + (date < 10 ? ('0' + date) : date)
  return s
}

function getAllDate (start, end) {
  const format = (time) => {
    let ymd = ''
    let mouth = (time.getMonth() + 1) >= 10 ? (time.getMonth() + 1) : ('0' + (time.getMonth() + 1))
    let day = time.getDate() >= 10 ? time.getDate() : ('0' + time.getDate())
    ymd += time.getFullYear() + '-' // 获取年份。
    ymd += mouth + '-' // 获取月份。
    ymd += day // 获取日。
    return ymd // 返回日期。
  }
  let dateArr = []
  let startArr = start.split('-')
  let endArr = end.split('-')
  let db = new Date()
  db.setUTCFullYear(startArr[0], startArr[1] - 1, startArr[2])
  let de = new Date()
  de.setUTCFullYear(endArr[0], endArr[1] - 1, endArr[2])
  let unixDb = db.getTime()
  let unixDe = de.getTime()
  let stamp
  const oneDay = 24 * 60 * 60 * 1000
  for (stamp = unixDb; stamp <= unixDe;) {
    dateArr.push(format(new Date(parseInt(stamp))))
    stamp = stamp + oneDay
  }
  return dateArr
}

export default {
  now, // 当前日期
  nowDayOfWeek, // 今天本周的第几天
  nowDay, // 当前日
  nowMonth: nowMonth + 1, // 当前月
  nowYear, // 当前年
  yesterday: getDay(-1, '-'), // 昨天日期
  getQuarterStartMonth: getQuarterStartMonth() + 1, // 獲取本季度開始月份
  getMonthStartDate: getCurrMonthDays().starttime, // 獲取本月開始日期
  getMonthEndDate: getCurrMonthDays().endtime, // 獲取本月結束日期
  getWeekStartDate: getTime(1), // 獲取本周開始日期 周日開始
  getWeekEndDate: getTime(-5), // 獲取本周結束日期 周六結束
  getLastWeekStartDate: getTime(8), // 獲取上周開始時間 周日開始
  getLastWeekEndDate: getTime(2), // 獲取上周開始時間 周日結束
  getNextWeekStartDate: getTime(-6), // 獲取上周開始時間 周日開始
  getNextWeekendDate: getTime(-12), // 獲取上周開始時間 周日結束
  getLastMonthStartDate: getLastMonthDays().starttime, // 獲取上個月開始日期
  getLastMonthEndDate: getLastMonthDays().endtime, // 獲取上個月結束日期
  getQuarterStartDate: getQuarterStartDate(), // 獲取本季度開始日期
  getQuarterEndDate: getQuarterEndDate(), // 獲取本季度結束日期
  formatDate, // date(時間對象，格式分割符)
  getMonthDays, // 獲取某月的天數
  getDay, // 獲取相對當前日期多少天的日期
  getTime, // 获取上周起始时间结束时间、下周起始时间结束时间开始时间和本周起始时间结束时间
  getAllDate // 獲取指定的時間段
}
