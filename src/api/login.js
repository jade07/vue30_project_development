// 引入自定义请求拦截器
import service from '@/utils/request.js';
/**
 * 登录
 */
export function Login (data) {
  return service.request({
    method: 'post',
    url: '/api/main/login',
    data: data
  });
}

/**
 * 获取验证码
 */
export function GetCode (data) {
  return service.request({
    method: 'post',
    url: '/api/main/getCode',
    data: data
  });
}

/**
 * 注册
 */
export function Register (data) {
  return service.request({
    method: 'post',
    url: '/api/main/register',
    data: data
  });
}

